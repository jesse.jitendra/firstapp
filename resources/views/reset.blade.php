@extends('layout')

@section('content')

<br><br><br>

<div class="row">
	<div class="col">
	
	</div>
	<div class="col border">

		<form method="post" action="/resetsub">
			<center><h4><i>Reset Password </i>  </h4>  </center>
			<br>
			@csrf
			<div class="form-group">
				<label>Old password</label>
				<input type="password"  name="opass" class="form-control">
				<span style="color: red;">{{ $errors->first('opass') }} </span>
			</div>
			<div class="form-group">
				<label>New Password</label>
				<input type="password" name="npassword" class="form-control">
				<span style="color: red;">{{ $errors->first('npassword') }} </span>
			</div>
            <div class="form-group">
            	<label>Confrim Password</label>
                <input type="password" name="cpassword" class="form-control">
                <span style="color: red;">{{ $errors->first('cpassword') }} </span>
            </div>
            <input type="hidden" name="email" value="{{ Auth::user()->email }}">
			
			<center>
			<button class="btn btn-primary" type="submit">Reset</button>
		</center>
		</form>
		<br>
	     @if(Session::get('notmatched') != null  )
          <div class="alert alert-danger">
          	{{ Session::get('notmatched') }} <button class="btn btn-default" data-dismiss="alert">x</button>
          </div>
	     @endif
		<br><br>
	</div>
	<div class="col"></div>
</div>


@endsection