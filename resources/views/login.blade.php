@extends('layout')

@section('content')

<br><br><br>

<div class="row">
	<div class="col">
       @if(Session::get('success'))
		<div class="alert alert-success">
			{{ Session::get('success') }}
			<button class="btn btn-default" data-dismiss='alert'>x</button>
		</div>
		@endif
	</div>
	<div class="col border">
		
		<form method="post" action="/log"> 
			
			<center><h4><i>Sign In</i>  </h4>  </center>
			<br>
			@csrf
			<div class="form-group">
				<label>Enter Email</label>
				<input type="email" name="email" class="form-control" value="{{ old('email') }}">
				<span style="color:red;">{{ $errors->first('email') }} </span>
			</div>
			<div class="form-group">
				<label>Enter Password</label>
				<input type="Password" name="password" class="form-control" value="{{ old('password') }}">
				<span style="color:red">{{ $errors->first('password') }}  </span>
			</div>
			<div class="form-group">
				<input type="checkbox" name="remember_me"> <label>Remember Me</label>  
			</div>
			<center>
			<button class="btn btn-primary" type="submit">submit</button>
		</center>
		</form>
		@if(Session::get('notfound'))
            <div class="alert alert-danger">{{ Session::get('notfound') }} <button class="btn btn-default" data-dismiss='alert'>x</button> </div>
		@endif
	    @if(Session::get('notmatched') != null )

	    <div class="alert alert-danger">
	    	{{ Session::get('notmatched') }}
	    	<button class="btn btn-default" data-dismiss='alert'>x</button>
	    </div>   

	    @endif
		<br><br>
	</div>
	<div class="col"></div>
</div>


@endsection