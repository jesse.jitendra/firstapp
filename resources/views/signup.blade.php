@extends('layout')

@section('content')

<br><br><br>

<div class="row">
	<div class="col"></div>
	<div class="col border">
		<form method="post" action="/sign">
			<center><h4><i>Sign Up</i>  </h4>  </center>
			<br>
			@csrf
			<div class="form-group">
				<label>Enter User name</label>
				<input type="text" name="username" class="form-control" value="{{ old('username') }}  ">
				<span style="color:red;">{{ $errors->first('username') }}  </span>
			</div>
			<div class="form-group">
				<label>Enter Email</label>
				<input type="email" name="email" class="form-control" value="{{ old('email') }} ">
				<span style="color: red;">{{ $errors->first('email') }} </span>
			</div>
			<div class="form-group">
				<label>Enter Password</label>
				<input type="Password" name="npassword" class="form-control" value="{{ old('npassword') }}">
				<span style="color: red;">{{ $errors->first('npassword') }}</span>
			</div>
			<div class="form-group">
				<label>Confirm Password</label>
				<input type="Password" name="cpassword" class="form-control" value="{{ old('cpassword') }}">
				<span style="color: red;">{{ $errors->first('cpassword') }}</span>
			</div>
			<center>
			<button class="btn btn-primary" type="submit">submit</button>
		</center>
		</form>
		
		<br><br>
	</div>
	<div class="col"></div>
</div>


@endsection