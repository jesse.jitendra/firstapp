@extends('layout')

@section('content')

<br><br><br>

<div class="row">
	<div class="col">
		<a href="/reset" class="btn btn-primary ">reset password</a>
	</div>
	<div class="col border">

		<form method="post" action="/update">
			<center><h4><i>User Profile</i>  </h4>  </center>
			<br>
			@csrf

			<div class="form-group">
				<label> User name</label>
				<input type="text" name="username" class="form-control" value="<?=  $query->name; ?> ">
				<span style="color:red;">@if($errors->first('username') != null ) 
			    {{ $errors->first('username') }}	
			     @endif    </span>
			</div> 
			<div class="form-group">
				<label> Email</label>
				<input type="email" name="email" class="form-control" value="<?= $query->email ?>" >
                <span style="color: red;">{{ $errors->first('email')  }}  </span>				
			</div>
		    <input type="hidden" name="id" value="<?= $query->id ?>">
			<center>
			<button class="btn btn-primary" type="submit">update</button>
		</center>
		</form>
		
		<br><br>
	</div>
	<div class="col"></div>
</div>


@endsection