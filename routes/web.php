<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});




// start middleware group function for restrict unlogged user 
Route::group(['middleware'=> ['test']], function() {

Route::get('/reset',function() {
	return view('reset');
});

Route::any('/profile','profile@index');

});

Route::any('/logout','signin@logout');

// end of middleware group function 

// function for group middleware for login user 
Route::group(['middleware'=> ['checkauth']], function() {

Route::get('/login',function(){
   return view('login');

});

Route::get('/signup',function(){
   return view('signup');
});

});
// endfunction 


Route::post('/sign','signup@index');
Route::post('/log','signin@authenticate');

Route::post('/update','profile@updateprofile');
Route::post('/resetsub','resetpass@index');
