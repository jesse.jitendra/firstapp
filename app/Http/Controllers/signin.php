<?php

namespace App\Http\Controllers;

use Validator; 
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
// use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Validator;
use user;


class signin extends Controller
{

 // function headergit 
  public function authenticate(Request $request)
    {
      $validator = validator::make($request->all(), [
          'email'=>'required|email',
          'password'=>'required'
      ]);
    
      
      if ($validator->fails()) {
        return redirect('/login')
                ->withErrors($validator)
                ->withInput();

      }else{

          $remember = false;
         if($request->input('remember_me') != null){
           $remember = true;
         }

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials,$remember)) {
        
              return   redirect('/');
        }else{
          $request->session()->flash('notmatched','Invalid details');
             return redirect('/login');
        }

      }
     
    }
    // end of login function 
    
    // function for logout
    public function logout(Request $request)
    {
       Auth::logout();
       Session::flush(); 
    	 return redirect('/');
    }
}
