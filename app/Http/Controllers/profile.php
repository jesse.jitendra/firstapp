<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\postlogin;

class profile extends Controller
{
    //function is to retrive user details from database 
    public function index(Request $request)
    {
       $email = Auth::user()->email;  
       $query = postlogin::where('email',$email)->first();

       return view('profile',['query'=>$query]  );
    }

    // function for update the user profile 
    public function updateprofile(Request $request)
    {
        
        $request->validate([
           'username'=>'required',
           'email'=>'email|required'
        ]);

        $id = $request->input('id');

        $query = postlogin::where('id',$id)
                 ->update([ 'email'=>$request->input('email'),'name'=>$request->input('username')]);

        if($query == true)
        {
           $request->session()->flash('updated','Profile Updated successfully');
            return redirect('/'); 

        }else{
          echo "updation failed";
        }          
    }
    // end function 
}
