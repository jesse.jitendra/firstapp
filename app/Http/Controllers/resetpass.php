<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class resetpass extends Controller
{
    // function for reset password 
    public function index(Request $request)
    {
    	$request->validate([
         'opass'=>'required',
         'npassword'=>'required',
         'cpassword'=>'required|same:npassword'
    	]);

    	 $email = $request->input('email');
    	

    	$query = DB::table('users')
    	         ->where('email',$email)
    	         ->get()->first();

    	$strored_pass =  $query->password;

    	if(Hash::check($request->input('opass'), $strored_pass))
    	{
    	   $hashedpass = Hash::make($request->input('cpassword'));
           $sql = DB::table('users')
                 ->where('email',$email)
                 ->update(['password'=>$hashedpass] );

                $request->session()->pull('user');
                 return redirect('/login');
    	}
    	else
    	{
    		$request->session()->flash('notmatched', "Old Password not matched");
    		return redirect('/reset');
    	}       

    }
}
