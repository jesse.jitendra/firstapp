<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\postlogin;

class signup extends Controller
{
    // function for signup to getting all details from user form and inserting the data in database
    public function index(Request $request)
    {
    	$request->validate([
    		'username'=>'required|min:5',
    		'email'=>'email|required|unique:users',
    		'npassword'=>'required',
    		'cpassword'=>'required|same:npassword'
    	]);
       
        $token = $request->input('_token');
        
    	$name = $request->input('username');
    	$email = $request->input('email');
    	$password = $request->input('cpassword');

    	$hashedpassword = Hash::make($password);
    	
    	$query =  postlogin::insert('insert into users(name,email,password,remember_token) values ("'.$name.'", "'.$email.' ", "'. $hashedpassword.'","'.$token.'" )', [1, 'Dayle']);
    	if($query == true){

    		$data = $request->session()->flash('success',"Sign up success please login now ");
            return redirect('/login');
    	}else{
    		echo "Something went wrong";
    	}

    }
}
