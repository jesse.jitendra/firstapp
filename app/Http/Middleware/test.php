<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;

// use Auth;
use Closure;

class test
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {

        }else{
          return redirect('/');
        }
        return $next($request);
    }
}
